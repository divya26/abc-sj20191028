# Angular Boot Camp

[Angular Boot Camp Curriculum](https://github.com/angularbootcamp/abc)

[Angular Boot Camp Zip File](http://angularbootcamp.com/abc.zip)

[Video Manager](http://videomanager.angularbootcamp.com)

[Workshop Repository](https://bitbucket.org/od-training/abc-sj20191028)

[Web Events](https://developer.mozilla.org/en-US/docs/Web/Events)

[Video Data](https://api.angularbootcamp.com/videos)

## VSCode Extensions

[Angular Language Service](https://marketplace.visualstudio.com/items?itemName=Angular.ng-template)

[angular2-inline](https://marketplace.visualstudio.com/items?itemName=natewallace.angular2-inline)

[TSLint](https://marketplace.visualstudio.com/items?itemName=ms-vscode.vscode-typescript-tslint-plugin)

## Utilities

[json2ts (generate TypeScript interfaces from JSON)](http://www.json2ts.com/)

[Moment.js Date library](https://momentjs.com/)

[date-fns Date library](https://date-fns.org/)
